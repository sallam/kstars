#include "domeoverlay.h"
#include "kstarsdata.h"
#include "linelist.h"
#include "skypoint.h"

#include <kstars.h>

DomeOverlay::DomeOverlay()
{
    rightLimit.setD(0);
    leftLimit.setD(0);
    lineList = std::make_shared<LineList>();

    std::shared_ptr<SkyPoint> p1(new SkyPoint());
    lineList->append(std::move(p1));

    std::shared_ptr<SkyPoint> p2(new SkyPoint());
    lineList->append(std::move(p2));

    std::shared_ptr<SkyPoint> p3(new SkyPoint());
    lineList->append(std::move(p3));

    std::shared_ptr<SkyPoint> p4(new SkyPoint());
    lineList->append(std::move(p4));
}

std::shared_ptr<LineList> DomeOverlay::get()
{
    return lineList;
}

void DomeOverlay::setAz(dms az)
{
    this->az = az;

    calculateLimits();
}

void DomeOverlay::setAz(double az)
{
    this->az.setD(az);

    calculateLimits();
}

void DomeOverlay::setHalfShutterCentralAngle(double shutterWidth, double domeRadius)
{
    this->halfShutterCentralAngle.setD((shutterWidth / domeRadius) * (180 / M_PI / 2));

    calculateLimits();
}

void DomeOverlay::calculateLimits()
{
    rightLimit = az - halfShutterCentralAngle;
    leftLimit = az + halfShutterCentralAngle;
    qDebug() << "azPos " << az.Degrees() << '\n';
    KStarsData *data = KStars::Instance()->data();

    lineList->at(0)->setAz(leftLimit);
    lineList->at(0)->setAlt(0);
    lineList->at(0)->HorizontalToEquatorial(data->lst(), data->geo()->lat());

    lineList->at(1)->setAz(rightLimit);
    lineList->at(1)->setAlt(0);
    lineList->at(1)->HorizontalToEquatorial(data->lst(), data->geo()->lat());

    lineList->at(2)->setAz(0);
    lineList->at(2)->setAlt(89.9);
    lineList->at(2)->HorizontalToEquatorial(data->lst(), data->geo()->lat());

    lineList->at(3)->setAz(leftLimit);
    lineList->at(3)->setAlt(0);
    lineList->at(3)->HorizontalToEquatorial(data->lst(), data->geo()->lat());
}
