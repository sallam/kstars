#ifndef DOMEOVERLAY_H
#define DOMEOVERLAY_H

#include "dms.h"
#include <memory>

class LineList;

class DomeOverlay
{
    public:
        DomeOverlay();
        void setAz(dms az);
        void setAz(double az);
        void setHalfShutterCentralAngle(double shutterWidth, double domeRadius);
        std::shared_ptr<LineList> get();

    private:
        std::shared_ptr<LineList> lineList;

        dms az;
        dms halfShutterCentralAngle;

        dms rightLimit;
        dms leftLimit;

        void calculateLimits();
};

#endif // DOMEOVERLAY_H
