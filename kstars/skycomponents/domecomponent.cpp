#include "domecomponent.h"
#include "klocalizedstring.h"
#include "kstars.h"
#include "kstarsdata.h"
#include "skymap.h"

#include <QColor>
#include <QBrush>

/*
 * wrong:
( 22.7967 , 8.66219 )

( 12.7651 , -8.66219 )

( 287.781 , 29.8514 )

( 22.7967 , 8.66219 )

 * correct:
( 22.9082 , 8.66219 )

( 12.8765 , -8.66219 )

( 287.892 , 29.8514 )

( 22.9082 , 8.66219 )
*/
DomeComponent::DomeComponent(SkyComposite *parent)
    : NoPrecessIndex(parent, i18n("Dome Componenet"))
{
    // TODO: It'll be much fun if I init the points here with their values and create update function which called whenever the dome changed.
    intro();
    rightLimit.setD(0);
    leftLimit.setD(0);
    lineList = std::make_shared<LineList>();
    // KStarsData *data = KStars::Instance()->data();

    std::shared_ptr<SkyPoint> p1(new SkyPoint(/*207.89, 60.1486*/));
    // p1->setAz(0);
    // p1->setAlt(0);
    // p1->HorizontalToEquatorial(data->lst(), data->geo()->lat());
    // qDebug() << '(' << p1->ra().Hours() << ',' << p1->dec().Degrees() << ")\n";
    lineList->append(std::move(p1));

    std::shared_ptr<SkyPoint> p2(new SkyPoint(/*117.89, 3.04286e-15*/));
    // p2->setAz(90);
    // p2->setAlt(0);
    // p2->HorizontalToEquatorial(data->lst(), data->geo()->lat());
    // qDebug() << '(' << p2->ra().Hours() << ',' << p2->dec().Degrees() << ")\n";
    lineList->append(std::move(p2));

    std::shared_ptr<SkyPoint> p3(new SkyPoint(/*27.8898, 29.8514*/));
    // p3->setAz(0);
    // p3->setAlt(90);
    // p3->HorizontalToEquatorial(data->lst(), data->geo()->lat());
    // qDebug() << '(' << p3->ra().Hours() << ',' << p3->dec().Degrees() << ")\n";
    lineList->append(std::move(p3));

    std::shared_ptr<SkyPoint> p4(new SkyPoint(/*207.89, 60.1486*/));
    // p4->setAz(0);
    // p4->setAlt(0);
    // p4->HorizontalToEquatorial(data->lst(), data->geo()->lat());
    // qDebug() << '(' << p4->ra().Hours() << ',' << p4->dec().Degrees() << ")\n";
    lineList->append(std::move(p4));

    appendBoth(lineList);
}

bool DomeComponent::selected()
{
    return true;
}

void DomeComponent::setAz(dms az)
{
    this->az = az;

    calculateLimits();
}

void DomeComponent::setAz(double az)
{
    this->az.setD(az);

    calculateLimits();
}

void DomeComponent::setHalfShutterCentralAngle(double shutterWidth, double domeRadius)
{
    this->halfShutterCentralAngle.setD((shutterWidth / domeRadius) * (180 / M_PI / 2));

    calculateLimits();
}

void DomeComponent::preDraw(SkyPainter *skyp)
{
    QColor color = KStarsData::Instance()->colorScheme()->colorNamed("CBoundColor");
    skyp->setPen(QPen(QBrush(color), 3, Qt::SolidLine));

    KStarsData *data = KStars::Instance()->data();

    lineList->at(0)->setAz(leftLimit);
    lineList->at(0)->setAlt(0);
    lineList->at(0)->HorizontalToEquatorial(data->lst(), data->geo()->lat());

    lineList->at(1)->setAz(rightLimit);
    lineList->at(1)->setAlt(0);
    lineList->at(1)->HorizontalToEquatorial(data->lst(), data->geo()->lat());

    lineList->at(2)->setAz(0);
    lineList->at(2)->setAlt(89.9);
    lineList->at(2)->HorizontalToEquatorial(data->lst(), data->geo()->lat());

    lineList->at(3)->setAz(leftLimit);
    lineList->at(3)->setAlt(0);
    lineList->at(3)->HorizontalToEquatorial(data->lst(), data->geo()->lat());

    qDebug() << '(' << lineList->at(0)->ra().Degrees() << ',' << lineList->at(0)->dec().Degrees() << ")\n";
    qDebug() << '(' << lineList->at(1)->ra().Degrees() << ',' << lineList->at(1)->dec().Degrees() << ")\n";
    qDebug() << '(' << lineList->at(2)->ra().Degrees() << ',' << lineList->at(2)->dec().Degrees() << ")\n";
    qDebug() << '(' << lineList->at(3)->ra().Degrees() << ',' << lineList->at(3)->dec().Degrees() << ")\n";
}

void DomeComponent::calculateLimits()
{
    rightLimit = az - halfShutterCentralAngle;
    leftLimit = az + halfShutterCentralAngle;
}
