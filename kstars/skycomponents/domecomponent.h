#ifndef DOMECOMPONENT_H
#define DOMECOMPONENT_H

#include "noprecessindex.h"
#include "skypainter.h"
#include "linelist.h"


class DomeComponent : public NoPrecessIndex
{
    public:
        /**
         * @short Constructor
         *
         * @p parent pointer to the parent SkyComposite object
         * name is the name of the subclass
         */
        explicit DomeComponent(SkyComposite *parent);
        virtual ~DomeComponent() override = default;
        bool selected() override;
        void setAz(dms az);
        void setAz(double az);
        void setHalfShutterCentralAngle(double shutterWidth, double domeRadius);

    protected:
        void preDraw(SkyPainter *skyp) override;

    private:
        std::shared_ptr<LineList> lineList;

        dms az;
        dms halfShutterCentralAngle;

        dms rightLimit;
        dms leftLimit;

        void calculateLimits();
};

#endif // DOMECOMPONENT_H
